# Homelab-Wiki

Wiki for my Homelab.

## Table of Content

<!-- TOC -->

- [Homelab-Terraform-vSphere](#homelab-terraform-vsphere)
  - [Table of Content](#table-of-content)
  - [Introduction and Goals](#introduction-and-goals)
  - [Hardware](#hardware)
  - [Software](#software)
    - [Ansible](#Ansible)
    - [Puppet](#Puppet)
    - [Terraform](#Terraform)
  - [Naming convention](#naming-convention)
  - [Lists](#lists)
  - [Sources](#sources)
  - [TODO](#todo)

<!-- /TOC -->

## Introduction and Goals

This wiki aims to be the table of truth for my homelab.

## Hardware

Here is an overview of the current hardware of the homelab.

![Alt text](./schemas/overview_homelab.png "Homelab Overview")

## Software

### Ansible

### Puppet

### Terraform

## Naming convention

**Virtual Machine**

All VM follow a specific naming convention that helps recognize them by their names.

       state         name     replicas
         |            |          |
    [lab-t-p][1-99][name][1-99][a-z]
               |           |
        project number  iteration

The state represents the current status of the virtual machine. The possible states are _lab_ for laboratory, _t_ for testing and _p_ for production. A laboratory machine does not contain any data and is rather use to properly deploy the testing and production environment with Terraform, Ansible and Puppet. The testing state might contain data and is used to test that the environment is working properly. The production state contains data and is the final state of a project.

The project number represents the project it is associated with (see #project_number).

The name represents what the virtual machine is or is doing (e.g. mariadb, okdnode,...).

The iteration is the generation of the virtual machine (e.g. p3mariadb2a is the second generation of mariadb)

The replicas is letter of the replication, _a_ is the first replication, _b_ the second and so on.

**Physical machine**

The physical machines follow the same naming convention as the virtual machine.

       state         name     replicas
         |            |          |
    [lab-t-p][1-99][name][1-99][a-z]
               |           |
        project number  iteration

The state represents the current status of the physical machine. The possible states are _lab_ for laboratory, _t_ for testing and _p_ for production. A laboratory machine does not contain any data and is rather use to properly install its OS. The testing state might contain data and is used to test that the environment is working properly (e.g. networking, storage...). The production state is the usable state of a physical machine.

The project number represents the project it is associated with. Most physical machine will use the project 1 as this is the common project number for all other project and that the physical machine will host virtual machines from different project.

The name represents what the physical machine is (e.g. esxi, san, rpi,...).

The iteration is the generation of the physical machine. This will mainly be 1 and will only increase if a physical machine is replaced by a new one rather than be added to the cluster.

The replicas is letter of the replication, _a_ is the first replication, _b_ the second and so on.

## Projects

Each projects has a number associate with them. This number is used in the name of the VM to easily identify to which project it is associate to.

### 1-9: Infrastructure

This project contains all physical and virtual machine needed for the homelab to run.

| NB  | Project           |
| --- | ----------------- |
| 1   | Physical machines |
| 2   | General tools     |
| 3   | Puppet            |
| 4   | Database          |
| 5   | Git               |

### 10-19: OS

This project contains all virtual machines with a specific OS (Linux, Windows, MAC,...) installed.

| NB  | Project    |
| --- | ---------- |
| 10  | Linux OS   |
| 11  | Windows OS |
| 12  | Mac OS     |

### 20-29: Virtualization

This project contains all machines used for virtualization like Docker, Kubernetes, OKD,...

| NB  | Project    |
| --- | ---------- |
| 20  | Docker     |
| 21  | Kubernetes |
| 22  | OKD        |
| 23  | Rancher    |

### 30-39: Machine learning

This project contains all machines used for machine learning.

### 40-49:

## Network reservation

### 192.168.1.20-.49: Backbone

| IP           | Name        | Type                    | Comments             |
| ------------ | ----------- | ----------------------- | -------------------- |
| 192.168.1.20 | p1ilo-01    | iLO port                | Proliant ML350 gen 9 |
| 192.168.1.21 | p1ilo-02    | iLO port                | Proliant ML110 gen 9 |
| 192.168.1.22 | p1proxmox1a | Proxmox Server 1A LAN 1 | Proliant ML350 gen 9 |
| 192.168.1.23 | p1proxmox1a | Proxmox Server 1A LAN 2 | Proliant ML350 gen 9 |
| 192.168.1.24 | p1proxmox1b | Proxmox Server 1B LAN 1 | PRoliant ML110 gen 9 |
| 192.168.1.25 | p1proxmox1c | Proxmox Server 1C LAN 1 | NUC                  |
| 192.168.1.26 |             |                         | FREE                 |
| 192.168.1.27 |             |                         | FREE                 |
| 192.168.1.28 |             |                         | FREE                 |
| 192.168.1.29 |             |                         | FREE                 |
| 192.168.1.30 |             |                         | FREE                 |
| 192.168.1.31 |             |                         | FREE                 |
| 192.168.1.32 |             |                         | FREE                 |
| 192.168.1.33 |             |                         | FREE                 |
| 192.168.1.34 |             |                         | FREE                 |
| 192.168.1.35 |             |                         | FREE                 |
| 192.168.1.36 |             |                         | FREE                 |
| 192.168.1.37 |             |                         | FREE                 |
| 192.168.1.38 |             |                         | FREE                 |
| 192.168.1.39 |             |                         | FREE                 |
| 192.168.1.40 |             |                         | FREE                 |
| 192.168.1.41 |             |                         | FREE                 |
| 192.168.1.42 |             |                         | FREE                 |
| 192.168.1.43 |             |                         | FREE                 |
| 192.168.1.44 |             |                         | FREE                 |
| 192.168.1.45 |             |                         | FREE                 |
| 192.168.1.46 |             |                         | FREE                 |
| 192.168.1.47 |             |                         | FREE                 |
| 192.168.1.48 |             |                         | FREE                 |
| 192.168.1.49 |             |                         | FREE                 |

### 192.168.1.50-99: Production reservation

| IP           | Name       | Type            | Comments      |
| ------------ | ---------- | --------------- | ------------- |
| 192.168.1.50 | p2tools1a  | Virtual machine | Tools         |
| 192.168.1.51 | p3puppet1a | Virtual machine | Puppet master |
| 192.168.1.52 |            |                 | FREE          |
| 192.168.1.53 |            |                 | FREE          |
| 192.168.1.54 |            |                 | FREE          |
| 192.168.1.55 |            |                 | FREE          |
| 192.168.1.56 |            |                 | FREE          |
| 192.168.1.57 |            |                 | FREE          |
| 192.168.1.58 |            |                 | FREE          |
| 192.168.1.59 |            |                 | FREE          |
| 192.168.1.60 |            |                 | FREE          |
| 192.168.1.61 |            |                 | FREE          |
| 192.168.1.62 |            |                 | FREE          |
| 192.168.1.63 |            |                 | FREE          |
| 192.168.1.64 |            |                 | FREE          |
| 192.168.1.65 |            |                 | FREE          |
| 192.168.1.66 |            |                 | FREE          |
| 192.168.1.67 |            |                 | FREE          |
| 192.168.1.68 |            |                 | FREE          |
| 192.168.1.69 |            |                 | FREE          |
| 192.168.1.70 |            |                 | FREE          |
| 192.168.1.71 |            |                 | FREE          |
| 192.168.1.72 |            |                 | FREE          |
| 192.168.1.73 |            |                 | FREE          |
| 192.168.1.74 |            |                 | FREE          |
| 192.168.1.75 |            |                 | FREE          |
| 192.168.1.76 |            |                 | FREE          |
| 192.168.1.77 |            |                 | FREE          |
| 192.168.1.78 |            |                 | FREE          |
| 192.168.1.79 |            |                 | FREE          |
| 192.168.1.80 |            |                 | FREE          |
| 192.168.1.81 |            |                 | FREE          |
| 192.168.1.82 |            |                 | FREE          |
| 192.168.1.83 |            |                 | FREE          |
| 192.168.1.84 |            |                 | FREE          |
| 192.168.1.85 |            |                 | FREE          |
| 192.168.1.86 |            |                 | FREE          |
| 192.168.1.87 |            |                 | FREE          |
| 192.168.1.88 |            |                 | FREE          |
| 192.168.1.89 |            |                 | FREE          |
| 192.168.1.90 |            |                 | FREE          |
| 192.168.1.91 |            |                 | FREE          |
| 192.168.1.92 |            |                 | FREE          |
| 192.168.1.93 |            |                 | FREE          |
| 192.168.1.94 |            |                 | FREE          |
| 192.168.1.95 |            |                 | FREE          |
| 192.168.1.96 |            |                 | FREE          |
| 192.168.1.97 |            |                 | FREE          |
| 192.168.1.98 |            |                 | FREE          |
| 192.168.1.99 |            |                 | FREE          |

### 192.168.1.100-149: Test reservation

| IP            | Name            | Type            | Comments           |
| ------------- | --------------- | --------------- | ------------------ |
| 192.168.1.100 | t3puppetagent1a | Virtual machine | Puppet agent       |
| 192.168.1.101 | t21master1a     | Virtual machine | K88s master node 1 |
| 192.168.1.102 | t21node1a       | Virtual machine | K8s node 1         |
| 192.168.1.103 | t21node1b       | Virtual machine | K8s node 2         |
| 192.168.1.104 |                 |                 | FREE               |
| 192.168.1.105 |                 |                 | FREE               |
| 192.168.1.106 |                 |                 | FREE               |
| 192.168.1.107 |                 |                 | FREE               |
| 192.168.1.108 |                 |                 | FREE               |
| 192.168.1.109 |                 |                 | FREE               |
| 192.168.1.110 |                 |                 | FREE               |
| 192.168.1.111 |                 |                 | FREE               |
| 192.168.1.112 |                 |                 | FREE               |
| 192.168.1.113 |                 |                 | FREE               |
| 192.168.1.114 |                 |                 | FREE               |
| 192.168.1.115 |                 |                 | FREE               |
| 192.168.1.116 |                 |                 | FREE               |
| 192.168.1.117 |                 |                 | FREE               |
| 192.168.1.118 |                 |                 | FREE               |
| 192.168.1.119 |                 |                 | FREE               |
| 192.168.1.120 |                 |                 | FREE               |
| 192.168.1.121 |                 |                 | FREE               |
| 192.168.1.122 |                 |                 | FREE               |
| 192.168.1.123 |                 |                 | FREE               |
| 192.168.1.124 |                 |                 | FREE               |
| 192.168.1.125 |                 |                 | FREE               |
| 192.168.1.126 |                 |                 | FREE               |
| 192.168.1.127 |                 |                 | FREE               |
| 192.168.1.128 |                 |                 | FREE               |
| 192.168.1.129 |                 |                 | FREE               |
| 192.168.1.130 |                 |                 | FREE               |
| 192.168.1.131 |                 |                 | FREE               |
| 192.168.1.132 |                 |                 | FREE               |
| 192.168.1.133 |                 |                 | FREE               |
| 192.168.1.134 |                 |                 | FREE               |
| 192.168.1.135 |                 |                 | FREE               |
| 192.168.1.136 |                 |                 | FREE               |
| 192.168.1.137 |                 |                 | FREE               |
| 192.168.1.138 |                 |                 | FREE               |
| 192.168.1.139 |                 |                 | FREE               |
| 192.168.1.140 |                 |                 | FREE               |
| 192.168.1.141 |                 |                 | FREE               |
| 192.168.1.142 |                 |                 | FREE               |
| 192.168.1.143 |                 |                 | FREE               |
| 192.168.1.144 |                 |                 | FREE               |
| 192.168.1.145 |                 |                 | FREE               |
| 192.168.1.146 |                 |                 | FREE               |
| 192.168.1.147 |                 |                 | FREE               |
| 192.168.1.148 |                 |                 | FREE               |
| 192.168.1.149 |                 |                 | FREE               |

### 192.168.1.150-199: Lab reservation

| IP            | Name | Type | Comments |
| ------------- | ---- | ---- | -------- |
| 192.168.1.150 |      |      | FREE     |
| 192.168.1.151 |      |      | FREE     |
| 192.168.1.152 |      |      | FREE     |
| 192.168.1.153 |      |      | FREE     |
| 192.168.1.154 |      |      | FREE     |
| 192.168.1.155 |      |      | FREE     |
| 192.168.1.156 |      |      | FREE     |
| 192.168.1.157 |      |      | FREE     |
| 192.168.1.158 |      |      | FREE     |
| 192.168.1.159 |      |      | FREE     |
| 192.168.1.160 |      |      | FREE     |
| 192.168.1.161 |      |      | FREE     |
| 192.168.1.162 |      |      | FREE     |
| 192.168.1.163 |      |      | FREE     |
| 192.168.1.164 |      |      | FREE     |
| 192.168.1.165 |      |      | FREE     |
| 192.168.1.166 |      |      | FREE     |
| 192.168.1.167 |      |      | FREE     |
| 192.168.1.168 |      |      | FREE     |
| 192.168.1.169 |      |      | FREE     |
| 192.168.1.170 |      |      | FREE     |
| 192.168.1.171 |      |      | FREE     |
| 192.168.1.172 |      |      | FREE     |
| 192.168.1.173 |      |      | FREE     |
| 192.168.1.174 |      |      | FREE     |
| 192.168.1.175 |      |      | FREE     |
| 192.168.1.176 |      |      | FREE     |
| 192.168.1.177 |      |      | FREE     |
| 192.168.1.178 |      |      | FREE     |
| 192.168.1.179 |      |      | FREE     |
| 192.168.1.180 |      |      | FREE     |
| 192.168.1.181 |      |      | FREE     |
| 192.168.1.182 |      |      | FREE     |
| 192.168.1.183 |      |      | FREE     |
| 192.168.1.184 |      |      | FREE     |
| 192.168.1.185 |      |      | FREE     |
| 192.168.1.186 |      |      | FREE     |
| 192.168.1.187 |      |      | FREE     |
| 192.168.1.188 |      |      | FREE     |
| 192.168.1.189 |      |      | FREE     |
| 192.168.1.190 |      |      | FREE     |
| 192.168.1.191 |      |      | FREE     |
| 192.168.1.192 |      |      | FREE     |
| 192.168.1.193 |      |      | FREE     |
| 192.168.1.194 |      |      | FREE     |
| 192.168.1.195 |      |      | FREE     |
| 192.168.1.196 |      |      | FREE     |
| 192.168.1.197 |      |      | FREE     |
| 192.168.1.198 |      |      | FREE     |
| 192.168.1.199 |      |      | FREE     |

### 192.168.1.200-254: Wifi and location reservation

| IP            | Name              | Type       | Comments  |
| ------------- | ----------------- | ---------- | --------- |
| 192.168.1.200 |                   |            | FREE      |
| 192.168.1.201 |                   |            | FREE      |
| 192.168.1.202 |                   |            | FREE      |
| 192.168.1.203 |                   |            | FREE      |
| 192.168.1.204 |                   |            | FREE      |
| 192.168.1.205 |                   |            | FREE      |
| 192.168.1.206 |                   |            | FREE      |
| 192.168.1.207 |                   |            | FREE      |
| 192.168.1.208 |                   |            | FREE      |
| 192.168.1.209 |                   |            | FREE      |
| 192.168.1.210 | vm1-ubuntu-server | Docker box | Legacy VM |
| 192.168.1.211 |                   |            | FREE      |
| 192.168.1.212 |                   |            | FREE      |
| 192.168.1.213 |                   |            | FREE      |
| 192.168.1.214 |                   |            | FREE      |
| 192.168.1.215 |                   |            | FREE      |
| 192.168.1.216 |                   |            | FREE      |
| 192.168.1.217 |                   |            | FREE      |
| 192.168.1.218 |                   |            | FREE      |
| 192.168.1.219 |                   |            | FREE      |
| 192.168.1.220 |                   |            | FREE      |
| 192.168.1.221 |                   |            | FREE      |
| 192.168.1.222 |                   |            | FREE      |
| 192.168.1.223 |                   |            | FREE      |
| 192.168.1.224 |                   |            | FREE      |
| 192.168.1.225 |                   |            | FREE      |
| 192.168.1.226 |                   |            | FREE      |
| 192.168.1.227 |                   |            | FREE      |
| 192.168.1.228 |                   |            | FREE      |
| 192.168.1.229 |                   |            | FREE      |
| 192.168.1.230 |                   |            | FREE      |
| 192.168.1.231 |                   |            | FREE      |
| 192.168.1.232 |                   |            | FREE      |
| 192.168.1.233 |                   |            | FREE      |
| 192.168.1.234 |                   |            | FREE      |
| 192.168.1.235 |                   |            | FREE      |
| 192.168.1.236 |                   |            | FREE      |
| 192.168.1.237 |                   |            | FREE      |
| 192.168.1.238 |                   |            | FREE      |
| 192.168.1.239 |                   |            | FREE      |
| 192.168.1.240 |                   |            | FREE      |
| 192.168.1.241 |                   |            | FREE      |
| 192.168.1.242 |                   |            | FREE      |
| 192.168.1.243 |                   |            | FREE      |
| 192.168.1.244 |                   |            | FREE      |
| 192.168.1.245 |                   |            | FREE      |
| 192.168.1.246 |                   |            | FREE      |
| 192.168.1.247 |                   |            | FREE      |
| 192.168.1.248 |                   |            | FREE      |
| 192.168.1.249 |                   |            | FREE      |
| 192.168.1.250 |                   |            | FREE      |
| 192.168.1.251 |                   |            | FREE      |
| 192.168.1.252 |                   |            | FREE      |
| 192.168.1.153 |                   |            | FREE      |
| 192.168.1.254 |                   |            | FREE      |
| 192.168.1.255 |                   |            | FREE      |

## TODO
